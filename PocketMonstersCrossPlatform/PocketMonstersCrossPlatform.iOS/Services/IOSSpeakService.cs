﻿using PocketMonstersCrossPlatform.Services;
using AVFoundation;
using PocketMonstersCrossPlatform.iOS.Services;

[assembly: Xamarin.Forms.Dependency(typeof(IOSSpeakService))]
namespace PocketMonstersCrossPlatform.iOS.Services
{
    public class IOSSpeakService : ISpeakService
    {
        public IOSSpeakService()
        {
        }

        public void Speak(string text)
        {
            var iosSpeechSyntherizer = new AVSpeechSynthesizer();
            var phraseToSpeak = new AVSpeechUtterance(text)
            {
                Rate = AVSpeechUtterance.MaximumSpeechRate / 4,
                Voice = AVSpeechSynthesisVoice.FromLanguage("en-US"),
                Volume = 0.5f,
                PitchMultiplier = 1.0f
            };

            iosSpeechSyntherizer.SpeakUtterance(phraseToSpeak);
        }
    }
}
