using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PocketMonstersCrossPlatform.Services;
using Android.Speech.Tts;
using Xamarin.Forms;
using PocketMonstersCrossPlatform.Droid.Services;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidSpeakService))]
namespace PocketMonstersCrossPlatform.Droid.Services
{
    public class AndroidSpeakService :
        Java.Lang.Object,
        TextToSpeech.IOnInitListener,
        ISpeakService
    {

        TextToSpeech androidSpeechService;
        string phraseToSpeak;

        public AndroidSpeakService()
        {
        }

        public void Speak(string text)
        {
            var androidContext = Forms.Context; // Instance of current android context. Include many Android SDK methods.
            phraseToSpeak = text;
            if (androidSpeechService == null)
            {
                androidSpeechService = new TextToSpeech(androidContext, this);
            }
            else
            {
                var parameters = new Dictionary<string, string>();
                androidSpeechService.Speak(phraseToSpeak, QueueMode.Flush, parameters);
            }
        }

        public void OnInit([GeneratedEnum] OperationResult status)
        {
            if (status.Equals(OperationResult.Success))
            {
                var parameters = new Dictionary<string, string>();
                androidSpeechService.Speak(phraseToSpeak, QueueMode.Flush, parameters);
            }
        }

    }
}