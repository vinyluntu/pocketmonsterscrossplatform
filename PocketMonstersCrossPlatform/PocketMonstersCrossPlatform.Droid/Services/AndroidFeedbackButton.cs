using PocketMonstersCrossPlatform.Services;
using Xamarin.Forms;
using PocketMonstersCrossPlatform.Droid.Services;
using HockeyApp.Android;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidFeedbackButton))]
namespace PocketMonstersCrossPlatform.Droid.Services
{
    public class AndroidFeedbackButton : IInitFeedbackButton
    {
        public Xamarin.Forms.Button InitFeedbackButton()
        {
            var androidContext = Forms.Context;

            var showFeedbackButton = new Xamarin.Forms.Button
            {
                Text = "Send feedback"
            };

            showFeedbackButton.Clicked += (sender, e) =>
            {
                //Register with the feedback manager
                FeedbackManager.Register(androidContext, AppConfig.HOCKEYAPP_APP_ID);

                //Show the feedback screen
                FeedbackManager.ShowFeedbackActivity(androidContext);
            };
            return showFeedbackButton;
        }
    }
}