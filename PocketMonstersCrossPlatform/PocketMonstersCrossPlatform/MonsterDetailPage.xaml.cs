﻿
using Newtonsoft.Json;
using PCLStorage;
using PocketMonstersCrossPlatform.Exceptions;
using PocketMonstersCrossPlatform.Services;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace PocketMonstersCrossPlatform
{
    public partial class MonsterDetailPage : ContentPage
    {

        List<Monster> myMonsterPocket = new List<Monster>();

        public MonsterDetailPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, true);
        }

        public MonsterDetailPage(Monster monster)
            : this()
        {
            this.BindingContext = monster;
        }

        public MonsterDetailPage(string monsterName)
           : this()
        {
            this.MonsterName = monsterName;
        }

        public string MonsterName { get; private set; }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFile file = await rootFolder.GetFileAsync("temp.txt");
            string json = await file.ReadAllTextAsync();
            myMonsterPocket = JsonConvert.DeserializeObject<List<Monster>>(json);

            yellMonsterName.IsEnabled = false;
            throwMonsterBall.IsEnabled = false;
            loadingIndicator.IsRunning = true;

            IPokeApi api = new PokeApiWebService();
            Monster monster = await api.GetMonster(this.MonsterName);

            loadingIndicator.IsRunning = false;
            yellMonsterName.IsEnabled = true;
            throwMonsterBall.IsEnabled = true;

            this.BindingContext = monster;
            Title = monster.Name;

            var feedbackButtonInit = DependencyService.Get<IInitFeedbackButton>();
            var button = feedbackButtonInit.InitFeedbackButton();

            // The following line cause a bug. When user return to detail page
            // the button is inserted second time as a children.
            ContentLayout.Children.Add(button);
        }

        private void OnMonsterYell(object sender, EventArgs e)
        {
            // HockeyApp Custom events.
            HockeyApp.MetricsManager.TrackEvent("User pressed OnMonsterYell button");

            var speakService = DependencyService.Get<ISpeakService>();

            if (!string.IsNullOrEmpty(this.MonsterName))
                speakService.Speak(MonsterName);
        }

        private async void OnThrowMonsterBall(object sender, EventArgs e)
        {
            // HockeyApp Custom events.
            HockeyApp.MetricsManager.TrackEvent("User pressed OnThrowMonsterBall button");

            await monsterImage.TranslateTo(-10, 0, 100);
            await monsterImage.TranslateTo(20, 0, 70);
            await monsterImage.TranslateTo(-20, 0, 70);
            await monsterImage.TranslateTo(10, 0, 100);

            myMonsterPocket.Add((Monster)this.BindingContext);

            var json = JsonConvert.SerializeObject(myMonsterPocket);

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFile file = await rootFolder.GetFileAsync("temp.txt");
            await file.WriteAllTextAsync(json);
        }

        private async void OnCrashApplication(object sender, EventArgs e)
        {
            // Custom exception class is created only test purposes. 
            // No need to create custom classes for exceptions. 
            // When application crashes, HockeyApp will take care of collecting
            // and sending crash reports. 
            throw new PocketMonsterException("This exception is only a demonstration how HockeyApp Crash Manager works!!!");
        }
        
    }
}
