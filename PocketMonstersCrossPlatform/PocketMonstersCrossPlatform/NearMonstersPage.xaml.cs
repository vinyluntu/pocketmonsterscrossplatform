﻿using PocketMonstersCrossPlatform.Services;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace PocketMonstersCrossPlatform
{
    public partial class NearMonstersPage : ContentPage
    {
        ObservableCollection<PaginatedMonsterListItem> nearMonsters = new ObservableCollection<PaginatedMonsterListItem>();

        public NearMonstersPage()
        {
            InitializeComponent();

            nearMonstersCollection.ItemsSource = nearMonsters;

            NavigationPage.SetHasNavigationBar(this, false);

            Device.OnPlatform(WinPhone: () =>
            {
                refreshNearMonsters.IsEnabled = true;
                refreshNearMonsters.IsVisible = true;
            });
        }

        async void OnListRefresh(object sender, EventArgs e)
        {
            // HockeyApp Custom events.
            HockeyApp.MetricsManager.TrackEvent("User refreshed NearMonsters list");

            IPokeApi api = new PokeApiWebService();

            nearMonsters.Clear();

           
            nearMonstersCollectionCount.Text = string.Empty;

            Random random = new Random();
            int randomOffset = random.Next(1, 500);
            var paginatedMonsterList = await api.GetList(randomOffset);

            foreach (PaginatedMonsterListItem monster in paginatedMonsterList.Results)
                nearMonsters.Add(monster);

            nearMonstersCollectionCount.Text = string.Format("Found {0} monsters", paginatedMonsterList.Results.Count.ToString());
            nearMonstersCollection.EndRefresh();
        }

        async void OnRefreshNearMonstersClicked(object sender, EventArgs e)
        {
            Device.OnPlatform(WinPhone: () =>
            {
                loadingIndicator.IsVisible = true;
                loadingIndicator.IsRunning = true;
            });

            nearMonstersCollectionCount.Text = string.Format("Yelling at monsters...");
            IPokeApi api = new PokeApiWebService();

            nearMonsters.Clear();

            Random random = new Random();
            int randomOffset = random.Next(1, 500);
            var paginatedMonsterList = await api.GetList(randomOffset);

            Device.OnPlatform(WinPhone: () =>
            {
                loadingIndicator.IsVisible = false;
                loadingIndicator.IsRunning = false;
            });

            foreach (PaginatedMonsterListItem monster in paginatedMonsterList.Results)
                nearMonsters.Add(monster);

            nearMonstersCollectionCount.Text = string.Format("Found {0} monsters", paginatedMonsterList.Results.Count.ToString());
        }

        async void OnListItemTapped(object sender, ItemTappedEventArgs e)
        {
            // HockeyApp Custom events.
            HockeyApp.MetricsManager.TrackEvent("User tapped OnListItemTapped item");

            if (e == null || e.Item == null)
                return;

            var listItem = (PaginatedMonsterListItem)e.Item;
            var monsterDetailPage = new MonsterDetailPage(listItem.Name);

            await Navigation.PushAsync(monsterDetailPage);
        }
    }
}
