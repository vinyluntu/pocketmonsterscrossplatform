﻿using Newtonsoft.Json;
using PCLStorage;
using PocketMonstersCrossPlatform.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace PocketMonstersCrossPlatform
{
    public partial class MyPocket : ContentPage
    {
        ObservableCollection<Monster> myMonsters = new ObservableCollection<Monster>();

        public MyPocket()
        {
            InitializeComponent();

            this.myMonstersCollection.ItemsSource = myMonsters;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            IFolder rootFolder = FileSystem.Current.LocalStorage;
            IFile file = await rootFolder.CreateFileAsync("temp.txt", CreationCollisionOption.OpenIfExists);

            string json = await file.ReadAllTextAsync();
            var myPocket = JsonConvert.DeserializeObject<List<Monster>>(json);

            myMonsters.Clear();

            foreach (Monster m in myPocket)
                myMonsters.Add(m);
        }
    }
}
