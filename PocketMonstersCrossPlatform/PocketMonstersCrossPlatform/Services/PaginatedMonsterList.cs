﻿using System.Collections.Generic;

namespace PocketMonstersCrossPlatform.Services
{
    public class PaginatedMonsterList 
    {
        public PaginatedMonsterList()
        {
            Results = new List<PaginatedMonsterListItem>();
        }

        public int Count { get; set; }
        public string Previous { get; set; }
        public List<PaginatedMonsterListItem> Results { get; set; }
        public string Next { get; set; }
    }

    public class PaginatedMonsterListItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
    }
}