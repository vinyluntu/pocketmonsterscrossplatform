﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace PocketMonstersCrossPlatform.Services
{
    public class PokeApiWebService : IPokeApi
    {
        HttpClient client;

        public async Task<PaginatedMonsterList> GetList()
        {
            InitializeHttpClient();
            Uri uri = new Uri("http://pokeapi.co/api/v2/pokemon");
            HttpResponseMessage response = await client.GetAsync(uri);

            return await ValidateAndDeserializePageResponse(response);
        }

        public async Task<PaginatedMonsterList> GetList(int offset)
        {
            if (offset < 0)
                throw new ArgumentException("Input error: offset must be greater or equal to zero.");

            InitializeHttpClient();
            Uri uri = new Uri(string.Format("http://pokeapi.co/api/v2/pokemon/?offset={0}", offset));
            HttpResponseMessage response = await client.GetAsync(uri);

            return await ValidateAndDeserializePageResponse(response);
        }

        public async Task<Monster> GetMonster(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("Input error: name was null.");

            InitializeHttpClient();
            Uri uri = new Uri(string.Format("http://pokeapi.co/api/v2/pokemon/{0}", name));
            HttpResponseMessage response = await client.GetAsync(uri);

            return await ValidateAndDeserializeMonsterResponse(response);
        }

        private void InitializeHttpClient()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        private async Task<PaginatedMonsterList> ValidateAndDeserializePageResponse(HttpResponseMessage response)
        {
            PaginatedMonsterList paginatedList = new PaginatedMonsterList();
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                paginatedList = JsonConvert.DeserializeObject<PaginatedMonsterList>(content);
            }
            return paginatedList;
        }

        private async Task<Monster> ValidateAndDeserializeMonsterResponse(HttpResponseMessage response)
        {
            Monster monster = new Monster();
            if (response.IsSuccessStatusCode)
            {
                string content = await response.Content.ReadAsStringAsync();
                monster = JsonConvert.DeserializeObject<Monster>(content);
            }
            return monster;
        }
    }
}
