﻿using Xamarin.Forms;

namespace PocketMonstersCrossPlatform.Services
{
    public interface IInitFeedbackButton
    {
       Button InitFeedbackButton();
    }
}
