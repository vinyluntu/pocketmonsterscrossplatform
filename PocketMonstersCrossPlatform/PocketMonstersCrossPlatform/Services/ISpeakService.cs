﻿namespace PocketMonstersCrossPlatform.Services
{
    public interface ISpeakService
    {
        void Speak(string text);
    }
}
