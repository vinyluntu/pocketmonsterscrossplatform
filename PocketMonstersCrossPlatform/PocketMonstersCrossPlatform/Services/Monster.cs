﻿namespace PocketMonstersCrossPlatform.Services
{
    public class Monster
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Base_experience { get; set; }
        public int Height { get; set; }
        public bool Is_default { get; set; }
        public int Order { get; set; }
        public int Weight { get; set; }
        public Sprites Sprites { get; set; }
    }

    public class Sprites
    {
        public string Back_female { get; set; }
        public string Back_shiny_female { get; set; }
        public string Back_default { get; set; }
        public string Front_female { get; set; }
        public string Front_shiny_female { get; set; }
        public string Back_shiny { get; set; }
        public string Front_default { get; set; }
        public string Front_shiny { get; set; }
    }
}