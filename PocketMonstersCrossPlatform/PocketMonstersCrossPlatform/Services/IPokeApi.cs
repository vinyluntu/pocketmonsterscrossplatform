﻿using System.Threading.Tasks;

namespace PocketMonstersCrossPlatform.Services
{
    public interface IPokeApi
    {
        Task<PaginatedMonsterList> GetList();
        Task<PaginatedMonsterList> GetList(int offset);

        Task<Monster> GetMonster(string name);
    }
}