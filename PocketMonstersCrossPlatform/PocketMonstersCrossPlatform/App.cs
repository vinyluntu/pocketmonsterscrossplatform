﻿using PCLStorage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace PocketMonstersCrossPlatform
{
    public class App : Application
    {
        public App()
        {
            MainPage = new NavigationPage(new PocketMonstersCrossPlatform.MainPage());
        }

        protected async override void OnStart()
        {
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            var exists = await rootFolder.CheckExistsAsync("temp.txt");

            if (exists != ExistenceCheckResult.FileExists)
            {
                IFile file = await rootFolder.CreateFileAsync("temp.txt", CreationCollisionOption.OpenIfExists);
                await file.WriteAllTextAsync("[]");
            }
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
