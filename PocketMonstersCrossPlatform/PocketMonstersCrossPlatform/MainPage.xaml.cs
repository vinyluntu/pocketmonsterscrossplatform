﻿
using Xamarin.Forms;

namespace PocketMonstersCrossPlatform
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
