﻿using System;

namespace PocketMonstersCrossPlatform.Exceptions
{
    public class PocketMonsterException : Exception
    {
        public PocketMonsterException(string message) : base(message)
        {
        }
    }
}
