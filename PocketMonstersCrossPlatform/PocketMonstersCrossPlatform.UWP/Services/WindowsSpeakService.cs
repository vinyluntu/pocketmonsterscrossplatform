﻿using PocketMonstersCrossPlatform.Services;
using PocketMonstersCrossPlatform.UWP.Services;
using System;
using Windows.UI.Xaml.Controls;

[assembly: Xamarin.Forms.Dependency(typeof(WindowsSpeakService))]
namespace PocketMonstersCrossPlatform.UWP.Services
{
    public class WindowsSpeakService : ISpeakService
    {
        public WindowsSpeakService()
        {

        }
        public async void Speak(string text)
        {
            var mediaElement = new MediaElement();
            var windowsSpeechSynthesis = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
            var stream = await windowsSpeechSynthesis.SynthesizeTextToStreamAsync(text);

            mediaElement.SetSource(stream, stream.ContentType);
            mediaElement.Play();
        }
    }
}
