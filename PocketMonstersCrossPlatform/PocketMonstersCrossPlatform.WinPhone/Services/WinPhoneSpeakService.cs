﻿using PocketMonstersCrossPlatform.Services;
using PocketMonstersCrossPlatform.WinPhone.Services;
using System;
using Windows.UI.Xaml.Controls;

[assembly: Xamarin.Forms.Dependency(typeof(WinPhoneSpeakService))]
namespace PocketMonstersCrossPlatform.WinPhone.Services
{
    public class WinPhoneSpeakService : ISpeakService
    {
        public WinPhoneSpeakService()
        {

        }
        public async void Speak(string text)
        {
            var mediaElement = new MediaElement();
            //var voice = SpeechSynthesizer.DefaultVoice;
            var windowsSpeechSynthesis = new Windows.Media.SpeechSynthesis.SpeechSynthesizer();
            var stream = await windowsSpeechSynthesis.SynthesizeTextToStreamAsync(text);

            mediaElement.SetSource(stream, stream.ContentType);
            mediaElement.Play();
        }
    }
}
